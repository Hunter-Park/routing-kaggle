import sys
from absl import flags
import os
from learning_to_route.env import Env as learning_to_route_env
from learning_to_route.util import evaluate_learning_to_route
from flow_avoidance.standard.env import Env as flow_avoidance_env
from flow_avoidance.util import evaluate_flow_avoidance
from link_hop.standard.env import Env as link_hop_env
from link_hop.gcn.env import Env as graph_link_hop_env
from link_hop.util import evaluate_link_hop
from stable_baselines.ddpg.policies import MlpPolicy as DDPGMlpPolicy
from stable_baselines.sac.policies import MlpPolicy as SACMlpPolicy
from stable_baselines.deepq.policies import MlpPolicy as DeepMlpPolicy
from stable_baselines.common.policies import MlpPolicy, MlpLnLstmPolicy
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import PPO2, DDPG, TRPO, A2C, SAC, DQN
from stable_baselines.common import BaseRLModel


FLAGS = flags.FLAGS
flags.DEFINE_string("environment", "link-hop-graph", "link-hop (learning to route), link-hop, or flow-avoidance")
flags.DEFINE_boolean("recurrent_policy", False, "Whether to use a recurrent policy")
flags.DEFINE_string("algorithm", "ppo", "Name of algorithm to use. Options are: a2c, ddpg, ppo, sac, DQN and trpo")
flags.DEFINE_integer("num_timesteps", 100_000, "Number of episodes to train the agent")
flags.DEFINE_string("save_directory", "run", "Name of directory to save rewards")
FLAGS(sys.argv)


def train(env_id=FLAGS.environment, algorithm_id=FLAGS.algorithm) -> (BaseRLModel, DummyVecEnv):
    if not os.path.exists(FLAGS.save_directory):
        os.makedirs(FLAGS.save_directory)

    env = None
    if env_id == "link-hop":
        env = DummyVecEnv([lambda: link_hop_env(FLAGS.save_directory + "/" + FLAGS.algorithm + ".csv")])
    elif env_id == "link-hop-graph":
        env = DummyVecEnv([lambda: graph_link_hop_env(FLAGS.save_directory + "/" + FLAGS.algorithm + ".csv",
                                                      max_nodes=60)])
    elif env_id == "ltr":
        env = DummyVecEnv([lambda: learning_to_route_env(FLAGS.save_directory + "/" + FLAGS.algorithm + ".csv")])
    elif env_id == "flow-avoidance":
        env = DummyVecEnv([lambda: flow_avoidance_env(FLAGS.save_directory + "/" + FLAGS.algorithm + ".csv")])
    else:
        print(f"Invalid environment parameter {FLAGS.environment}")
        quit()

    if FLAGS.recurrent_policy:
        if FLAGS.algorithm in ["ddpg", "sac", "trpo"]:
            print(f"{FLAGS.algorithm} does not support a recurrent policy!")
            quit()
        policy = MlpLnLstmPolicy
    else:
        policy = MlpPolicy

    model = None
    tensorboard_dir = FLAGS.save_directory + "/tensorboard/" + FLAGS.algorithm + "_tensorboard"
    if algorithm_id == "a2c":
        model = A2C(policy, env, verbose=1, tensorboard_log=tensorboard_dir)
    elif algorithm_id == "ddpg":
        model = DDPG(DDPGMlpPolicy, env, verbose=1, tensorboard_log=tensorboard_dir)
    elif algorithm_id == "ppo":
        model = PPO2(policy, env, verbose=1, nminibatches=1, tensorboard_log=tensorboard_dir)
    elif algorithm_id == "sac":
        model = SAC(SACMlpPolicy, env, verbose=1, tensorboard_log=tensorboard_dir)
    elif algorithm_id == "trpo":
        model = TRPO(policy, env, verbose=1, tensorboard_log=tensorboard_dir)
    elif algorithm_id == "dqn":
        model = DQN(DeepMlpPolicy, env, verbose=1, tensorboard_log=tensorboard_dir)
    else:
        print(f"Invalid algorithm parameter {FLAGS.algorithm}")
        quit()

    return model.learn(total_timesteps=FLAGS.num_timesteps), env


trained_model, environment = train(FLAGS.environment, FLAGS.algorithm)

if FLAGS.environment == 'link-hop':
    evaluate_link_hop(trained_model, environment)
elif FLAGS.environment == 'flow-avoidance':
    evaluate_flow_avoidance(trained_model, environment)
elif FLAGS.environment == 'ltr':
    evaluate_learning_to_route(trained_model, environment)
