# SLU Summer REU 2019
##### Student: Hunter Park
#####  Adviser: Dr Flavio Esposito

### Manual Link Hopping

This environment takes ["Learning to Route with Deep RL"](http://www.cs.huji.ac.il/~schapiram/Learning_to_Route%20(NIPS).pdf)
further and makes the agent manually control which neighbor the packet gets routed. Given 2 inputs, a one-hot of the current node 
that the packet is on, and a one-hot of the target node, and an output of a one-hot of the neighbors, the agent must select 
which neighbor to move to next. 

There is a temporary simplification of the problem: the degree of every node is the same: 5. The network used is a icosahedral graph,
which has 12 vertices and 30 edges. The weights of this graph are randomly set at each runtime.

In order to generalize this to a non symmetric graph, I have to figure out how to mask the gradients that stable-baselines computes.
It might not be possible, which will then require a custom implementation of A2C, TRPO, and PPO.

##### Rewards
The reward structure is sparse, meaning that it only receives a reward at the end of the episode: +r if the agent successfully 
routed the packet to the target destination, and -1 if the agent failed to route after n hops, where n = diameter(graph) + 2.

| Game State | Reward |
| --- | --- |
| Failed to route | -1 |
| Successful routing | r |

r is a the sum of the fractions of best paths from source -> target for each set of edge weights. 

![Image of Yaktocat](../figures/link_hop_discrete.png)

We tried a recurrent policy as well

![Image of Yaktocat](../figures/link_hop_recurrent_discrete.png)

We observed another reward structure: if the agent succesfully routed the packet, it's reward is the fraction: shortest path
computed from A* / length of path the agent took. The final trained PPO model was able to route the optimal path 59 out of 85 routes,
and didn't drop a packet.

![Image of Yaktocat](../figures/link_hop_continuous.png)

We tried a recurrent NN, and observed the results below on the non scaled reward

![Image of Yaktocat](../figures/link_hop_recurrent_continuous.png)