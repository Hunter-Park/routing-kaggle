from link_hop.util import create_graph, get_new_route
from link_hop.gcn.util import create_obs, permute
from helper.graph import get_max_neighbors, get_neighbors, draw_graph
from gym.spaces import Box, Discrete
from networkx import adjacency_matrix, to_numpy_matrix
import numpy as np
from reward import compute_reward


class Env(object):
    # Constructor, create graphs, set some variables for gym, house keeping stuff
    def __init__(self, save_file: str, max_nodes=60) -> None:
        # Create is created at episode begin, not at environment creation
        self.graph = create_graph(min_nodes=20, max_nodes=max_nodes, max_neighbors=None)

        # Declare the spaces for the Env
        self.max_neighbors = get_max_neighbors(self.graph)
        self.max_nodes = max_nodes

        self.observation_space: Box = Box(low=0, high=1, shape=(3, self.max_nodes, self.max_nodes), dtype=np.float32)
        self.action_space: Discrete = Discrete(self.max_neighbors)
        self.current_node: int = -1
        self.target_node: int = -1
        self.neighbors = None
        self.path: list = []
        self.valid_actions = [1] * self.max_neighbors

        # Utility variables
        self.save_file: str = save_file
        self.steps = 0
        self.metadata = None
        self.status: bool = False
        self.first_step = True

    # Preform the action (set the weights of the network graph)
    # Compute reward, log reward to file
    def step(self, action: int) -> (list, float, bool, dict):
        if self.first_step:
            action = 0
            self.first_step = False
        next_node = self.neighbors[action]

        self.path.append(next_node)
        self.current_node = next_node

        reward, done = self._get_reward()
        if done:
            with open(self.save_file, 'a') as fd:
                fd.write(str(self.steps) + ',' + str(reward) + '\n')
                fd.close()

        self.neighbors = get_neighbors(self.graph, self.current_node)
        self.valid_actions = [0] * self.max_neighbors
        for i, value in enumerate(self.neighbors):
            if i < self.max_neighbors:
                self.valid_actions[i] = 1
        self.steps += 1

        return create_obs(self.graph_adj, self.current_node, self.target_node), reward, done, \
               {'action_mask': self.valid_actions}

    # Called when an environment is finished, creates a new "environment"
    def reset(self):
        self.first_step = True
        return self._reset()

    # return an observation
    def _reset(self) -> np.array:
        # create a new graph for the next episode
        # self.graph = create_graph(min_nodes=20, max_nodes=self.max_neighbors, max_neighbors=self.max_neighbors)
        self.graph_adj = to_numpy_matrix(self.graph, nodelist=list(self.graph.nodes), weight='weight')
        # clean up some episode specific memory
        self.path.clear()

        # Get the first observation
        self.current_node, self.target_node = get_new_route(self.graph)
        self.neighbors = get_neighbors(self.graph, self.current_node)
        self.path.append(self.current_node)
        return create_obs(self.graph_adj, self.current_node, self.target_node)

    # not used / doesn't make sense to use given the problem
    def _render(self, mode='human', close=False) -> None:
        pass

    # compute the reward
    def _get_reward(self) -> (list, bool, float):
        if len(self.path) > 5 * len(list(self.graph.nodes)):
            return -10, True
        elif self.path[-1] == self.target_node:
            return 1, True
        return compute_reward(self.graph, self.target_node, self.path), False
