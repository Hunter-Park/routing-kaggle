import networkx as nx
import numpy as np
from random import shuffle


def pad(A, length):
    arr = np.zeros((length, length))
    for i in range(0, A.shape[0]):
        arr[i][:A.shape[0]] = A[i]
    return arr


def create_obs(adj_mat: np.array, curr_pos: int, target_pos: int) -> np.array:
    sparse_current = np.zeros((60, 60))
    # sparse_current = np.zeros(adj_mat.shape)
    sparse_current[curr_pos][curr_pos] = 1

    sparse_target = np.zeros((60, 60))
    # sparse_target = np.zeros(adj_mat.shape)
    sparse_target[target_pos][target_pos] = 1
    return np.stack([pad(adj_mat, 60), sparse_current, sparse_target], axis=0)


def all_in_dict(nodes: list, mapping) -> bool:
    for node in nodes:
        if node not in mapping.keys():
            return False
    return True


def permute(graph: nx.Graph) -> nx.Graph:
    mapping = {}
    new = list(graph.nodes)
    shuffle(new)
    for node, i in enumerate(list(graph.nodes)):
        mapping[node] = new[i]
    return nx.relabel_nodes(graph, mapping)
