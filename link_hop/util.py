import networkx as nx
from random import choice
from helper.graph import draw_graph, get_max_neighbors, get_node_with_max_neighbors, randomize_weights,\
    compute_path_length, compute_best_flow, compute_flow_value, set_max_neighbors
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines.common import BaseRLModel
from statistics import mean


# Create a graph
# icosahedral_graph has (12V, 30E)
# origin paper had 12 vertices and 32 edges
def create_graph(min_nodes=20, max_nodes=60, max_neighbors=None) -> nx.Graph:
    size_of_groups = 4
    rewire_prob = 0.5
    num_nodes = []
    for num in range(min_nodes, max_nodes, 4):
        num_nodes.append(num)
    nodes = choice(num_nodes)
    nodes = 60
    number_of_groups = nodes // size_of_groups
    graph = randomize_weights(nx.relaxed_caveman_graph(number_of_groups, size_of_groups, rewire_prob))

    while not nx.is_connected(graph):
        nodes = choice(num_nodes)
        number_of_groups = nodes // size_of_groups
        graph = randomize_weights(nx.relaxed_caveman_graph(number_of_groups, size_of_groups, rewire_prob))
    # draw_graph(graph)
    return graph


# Create a random route in the network
def get_new_route(graph: nx.Graph) -> (int, int):
    nodes = list(graph.nodes)
    node1 = get_node_with_max_neighbors(graph)
    node2 = choice(nodes)
    while node1 == node2:
        node2 = choice(nodes)
    return node1, node2


# Compute the reward
# Compute the fraction of the best path given that index of the edge weights
# Return the list of those fractions
def compute_reward(graph: nx.Graph, target: int, path: list) -> (list, bool):
    if path[-1] == target:
        # best_path_length = nx.astar_path_length(graph, path[0], target)
        # actual_path_length = compute_path_length(graph, path)
        # latency_reward = best_path_length / actual_path_length
        #
        # best_flow_value = compute_best_flow(graph, path[0], target)
        # actual_flow_value = compute_flow_value(graph, path)
        # flow_reward = actual_flow_value / best_flow_value
        return [10], True
    if len(path) > 5*len(list(graph.nodes)):
        return [-10], True
    else:
        try:
            if nx.astar_path_length(graph, path[-1], target) < nx.astar_path_length(graph, path[-2], target):
                return [0.2], False
            else:
                return [0], False
        except nx.exception.NetworkXNoPath as e:
            draw_graph(graph)
            print(e)


# Enjoy trained agent
def evaluate_link_hop(model: BaseRLModel, env: DummyVecEnv) -> None:
    good = 0
    bad = 0
    reward = 0
    for _ in range(200):
        obs, done, action_masks = env.reset(), False, []
        while not done:
            action, _states = model.predict(obs, action_mask=action_masks)
            obs, reward, done, infos = env.step(action)

            action_masks.clear()
            for info in infos:
                env_action_mask = info.get('action_mask')
                action_masks.append(env_action_mask)
        if reward > 0:
            good += 1
        else:
            bad += 1

    print(f"% Routed: {good / float(good + bad)}")

