from link_hop.util import create_graph, get_new_route, get_max_neighbors
from helper.graph import get_neighbors
from gym.spaces import MultiDiscrete, Discrete
from networkx import Graph
from reward import compute_reward
import gym


# Learning to Route as a gym environment with multiple objectives
class Env(gym.Env):
    # Constructor, create graphs, set some variables for gym, house keeping stuff
    def __init__(self, save_file: str, graph: Graph = None) -> None:

        # Create our graphs, each with a unique set of edge weights
        if graph is None:
            self.graph: Graph = create_graph()
        else:
            self.graph = graph

        # Declare the spaces for the Env
        self.max_neighbors = get_max_neighbors(self.graph)

        self.observation_space: MultiDiscrete = MultiDiscrete([self.num_nodes(), self.num_nodes()])
        self.action_space: Discrete = Discrete(self.max_neighbors)

        # Counters
        self.steps: int = 0
        self.hops: int = 0

        # Log dir
        self.save_file: str = save_file

        # Path information
        self.source: int = -1
        self.target: int = -1
        self.current_node: int = -1
        self.path: list = []
        self.neighbors = []

    # Preform the action and compute reward
    def step(self, action: int) -> ((int, int), float, bool, dict):
        next_node = self.neighbors[action]
        self.path.append(next_node)
        self.current_node = next_node
        self.hops += 1
        self.steps += 1
        rewards, done = self._get_reward()
        if done:
            with open(self.save_file, 'a') as fd:
                fd.write(str(self.steps))
                for reward in rewards:
                    fd.write(',' + str(reward))
                fd.write('\n')
                fd.close()
        valid_actions = [0] * self.max_neighbors
        self.neighbors = list(self.graph.neighbors(self.current_node))
        for i in range(len(self.neighbors)):
            valid_actions[i] = 1
        return [self.current_node, self.target], sum(rewards), done, {'action_mask': valid_actions,
                                                                      'rewards': rewards}

    # Called when an environment is finished, creates a new "environment"
    def reset(self) -> (int, int):
        return self._reset()

    # Reset counters to zero, get an observation store it and return it
    def _reset(self) -> (int, int):
        self.source, self.target = get_new_route(self.graph)
        self.current_node = self.source
        self.path = []
        self.neighbors = get_neighbors(self.graph, self.current_node)
        self.path.append(self.source)
        self.hops = 0

        return self.source, self.target

    # not used / doesn't make sense to use given the problem
    def _render(self, mode: str = 'human', close: bool = False) -> None:
        pass

    # compute the reward
    def _get_reward(self) -> (list, bool):
        if len(self.path) > 5 * len(list(self.graph.nodes)):
            return [-10], True
        elif self.path[-1] == self.target:
            return [1], True
        return compute_reward(self.graph, self.target, self.path), False

    def num_nodes(self) -> int:
        return len(self.graph.nodes)
