import networkx as nx
from random import choice
from stable_baselines.common import BaseRLModel
from stable_baselines.common.vec_env import DummyVecEnv
from helper.graph import draw_graph, get_node_with_max_neighbors, randomize_weights, compute_path_length
from statistics import mean, stdev

discount_factor = 1
number_of_groups = 4
size_of_groups = 10
rewire_prob = 0.5


# Create a graph that has 12 vertices and 30 edges
# origin paper had 12 vertices and 32 edges
def create_graph() -> nx.Graph:
    # graph = randomize_weights(nx.relaxed_caveman_graph(number_of_groups, size_of_groups, rewire_prob))
    # while not nx.is_connected(graph):
    #     graph = randomize_weights(nx.relaxed_caveman_graph(number_of_groups, size_of_groups, rewire_prob))
    # draw_graph(graph)
    # return graph
    return randomize_weights(nx.icosahedral_graph())


# Pick a source and target node for both flows
def get_new_route(graph: nx.Graph) -> (int, int, int, int):
    nodes = list(graph.nodes)
    node1 = get_node_with_max_neighbors(graph)
    node2 = choice(nodes)
    node3 = choice(nodes)
    node4 = choice(nodes)
    while node1 == node2:
        node2 = choice(nodes)
    while node3 == node4:
        node3 = choice(nodes)
        node4 = choice(nodes)
    return node1, node2, node3, node4


# Compute the reward of from the environment given a history of actions (the path)
def compute_reward(graph: nx.Graph, flow1_path: list, flow1_target: int, flow2_source: int, flow2_target: int) -> (
float, bool):
    # Too many hops, failed to route efficiently
    if len(flow1_path) > len(list(graph.nodes)):
        return -1, True, None
    # Not done, keep playing in the environment
    elif flow1_path[-1] != flow1_target:
        return 0, False, None
    else:
        reward = 1
        # Agent reached target
        flow2_path = nx.astar_path(graph, flow2_source, flow2_target)
        for node1 in flow1_path:
            for node2 in flow2_path:
                # flows collided
                if node1 == node2:
                    reward *= discount_factor

        return reward, True, \
               float(compute_path_length(graph, flow1_path)) / float(
                   nx.astar_path_length(graph, flow1_path[0], flow1_target))


# Enjoy trained agent
def evaluate_flow_avoidance(model: BaseRLModel, env: DummyVecEnv) -> None:
    path_bends = []
    rewards = []
    while len(path_bends) < 250:
        done = False
        obs = env.reset()
        info = None
        while not done:
            action, _states = model.predict(obs)
            obs, rewards, done, info = env.step(action)
        if rewards >= 0:
            path_bends.append(info[0]["path_bend"])

    print(f"Average path bend: {mean(path_bends)}")
    print(f"Std of path bends: {stdev(path_bends)}")
