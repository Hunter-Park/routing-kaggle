# SLU Summer REU 2019
##### Student: Hunter Park
#####  Adviser: Dr Flavio Esposito
### Collision Avoidance of Flows in a Network

This environment is the true start of the REU: flow collision avoidance in a software defined network.

This environment requires the agent to route a flow through a network, while trying to avoid another flow in the network.
The agent is given the current node its flow is at, the target node it needs to get to, and the start and target nodes of the 
other flow. 

The reward is: 
if agent took > diameter(graph) + 2 hops: -1
Successful routing but collided: 0
Successful routing without collision: 1

| Game State | Reward |
| --- | --- |
| Failed to route | -1 |
| Successful routing but collided | 0 |
| Successful routing without collision | 1|

With a vanilla NN, the awards are observed below

![Image of Yaktocat](../figures/flow_avoidance.png)

However, an agent would benefit greatly by having a recurrent layer, the rewards of a recurrent NN are observed below.

![Image of Yaktocat](../figures/flow_avoidance_recurrent.png)

While both model architectures end up converging to receive similar rewards, the recurrent models reach that point in roughly half the timesteps.

