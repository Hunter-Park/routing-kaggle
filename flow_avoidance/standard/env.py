from flow_avoidance.util import create_graph, get_new_route
from helper.graph import get_neighbors
from gym.spaces import MultiDiscrete, Discrete
from networkx import Graph
from helper.graph import get_max_neighbors
from reward import compute_reward_avoid


# Learning to Route as a gym environment
class Env(object):
    # Constructor, create graphs, set some variables for gym, house keeping stuff
    def __init__(self, save_file: str, graph: Graph = None) -> None:
        if graph is None:
            self.graph: Graph = create_graph()
        else:
            self.graph = graph
        self.status: bool = False
        # Declare the spaces for the Env
        self.max_neighbors = get_max_neighbors(self.graph)

        self.observation_space: MultiDiscrete = MultiDiscrete([len(self.graph.nodes), len(self.graph.nodes),
                                                               len(self.graph.nodes), len(self.graph.nodes)])
        self.action_space: Discrete = Discrete(self.max_neighbors)
        self.save_file: str = save_file
        self.current_node: int = -1
        self.flow1_target: int = -1
        self.flow2_source: int = -1
        self.flow2_target: int = -1
        self.neighbors: dict = dict()
        self.path: list = []
        self.steps = 0

        for node in self.graph:
            self.neighbors[node] = get_neighbors(self.graph, node)

        self.metadata = None

    # Preform the action (set the weights of the network graph)
    # Compute reward, log reward to file
    def step(self, action: int) -> (list, float, bool, dict):
        neighbors = list(self.graph.neighbors(self.current_node))
        next_node = neighbors[action]
        self.path.append(next_node)
        self.current_node = next_node
        self.steps += 1
        reward, done = self._get_reward()
        if done:
            with open(self.save_file, 'a') as fd:
                fd.write(str(self.steps) + ','+ str(reward) + '\n')
                fd.close()
        valid_actions = [0] * self.max_neighbors
        neighbors = list(self.graph.neighbors(self.current_node))
        for i in range(len(neighbors)):
            valid_actions[i] = 1

        return [self.current_node, self.flow1_target, self.flow2_source, self.flow2_target], reward, done, \
               {'action_mask': valid_actions}

    # Called when an environment is finished, creates a new "environment"
    def reset(self):
        return self._reset()

    # return an observation
    def _reset(self) -> (int, int, int, int):
        self.path.clear()
        self.current_node, self.flow1_target, self.flow2_source, self.flow2_target = get_new_route(self.graph)
        self.path.append(self.current_node)
        return self.current_node, self.flow1_target, self.flow2_source, self.flow2_target

    # not used / doesn't make sense to use given the problem
    def _render(self, mode='human', close=False) -> None:
        pass

    # compute the reward
    def _get_reward(self) -> (float, bool):
        if len(self.path) > 5 * len(list(self.graph.nodes)):
            return -10
        elif self.path[-1] == self.flow1_target:
            return 1, True
        return compute_reward_avoid(self.graph, self.path, self.flow1_target, self.flow2_source, self.flow2_target)
