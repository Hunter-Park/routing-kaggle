
# Reinforcement Learning Network Routing Kaggle Competition

Dr Flavio Esposito

### [](#requirements)Requirements
python3.7
`sudo pip install -r requirements.txt`
Please ensure that stable-baselines directory is set in python path before running train

### [](#introduction)Introduction

This project focused on exploring data driven approaches to network routing, in particular, routing protocols that are learned. The goal of the project is to learn a routing protocol for a network by training an reinforcement learning agent to manually make the decisions for every hop.

### Task
In the file `reward.py`, you will see two functions, ``compute_reward`` and ``compute_reward_avoid``. Your task is to carefully design a reward function such that an agent learns how to route in a large network effectively in as few time steps as possible. 

