# SLU Summer REU 2019
##### Student: Hunter Park
#####  Adviser: Dr Flavio Esposito
### [Learning to Route with Deep RL](http://www.cs.huji.ac.il/~schapiram/Learning_to_Route%20(NIPS).pdf) Recreation
The project started with a creation of a paper that tried to used Deep Reinforcement Learning (Deep-RL) to create a routing protocol within a software defined network. This approach an input of a series of demand matrices, and outputted per edge weights of the network that corresponded to the % of traffic that would be sent through that link.

From a particular node, they converted the edges from that node into a discrete probability distribution and selected an edge based on its probability. They coined this approach "Softmin Routing"

I could not get agents to converge using softmin routing, so I converted the values of the edges directly, instead of e<sup>value</sup>
I added another derivation, in which the packet will not go back to the previous node from which it came from. This prevents a packet from bouncing between two nodes indefinitely and failing to route the packet.

To run all the experiments in one go

```chmod +x scripts/run-learning-to-route.sh```

```./scripts/run.sh num_iterations```

This will train all 5 RL algorithms (DDPG, TRPO, A2C, PPO, SAC) at the same time, which takes up roughly 2gb of GPU, a few GB of memory, and a 5 cpu cores.
Then, it will graph the results and save it in "performance.png"
It takes 2-3 hours to run for 1M episodes.

To train an individual algorithm: ```./python train.py algorithm=ppo num_timesteps=1000000 save_directory=run```

where algorithm is one of the following: a2c, ddpg, ppo, sac, trpo
directory is relative to execution point directory

to plot indiviudal runs: ```python ./scrptsplot.py DIRECTORY SMOOTHING_WEIGHT```

where smoothing weight is on the interval [0,1) and directory is the same as specified in training

Tensorboard sessions are saved the specified save directory when training.

![Image of Yaktocat](../figures/learning_to_route.png)