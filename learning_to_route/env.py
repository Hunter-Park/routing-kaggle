from .util import create_graph, randomize_weights, set_weights, make_demand_matrix, compute_reward
from gym.spaces import Box
from networkx import Graph
import numpy as np
import gym


# Learning to Route as a gym environment
class Env(gym.Env):
    # Constructor, create graphs, set some variables for gym, house keeping stuff
    def __init__(self, save_file: str) -> None:
        self.real_graph: Graph = create_graph()
        self.network_graph: Graph = randomize_weights(self.real_graph)
        self.status = False

        num_edges = len(list(self.real_graph.edges))
        self.demand_matrix_size: int = 45  # must be divisible by 3
        self.observation_space: Box = Box(low=0, high=50, shape=(1, self.demand_matrix_size), dtype=np.int)
        self.action_space: Box = Box(low=0, high=1, shape=(num_edges, ), dtype=np.float16)
        self.steps: int = 0
        self.routes: np.array = np.zeros(self.demand_matrix_size)
        self.save_file: str = save_file

    # Preform the action (set the weights of the network graph)
    # Compute reward, log reward to file
    def step(self, action: np.ndarray) -> (np.array, float, bool, dict):
        set_weights(self.network_graph, action)
        reward = self._get_reward()
        self.steps += 1
        ob = make_demand_matrix(self.real_graph, self.demand_matrix_size)

        with open(self.save_file, 'a') as fd:
            fd.write(str(self.steps) + ',' + str(reward) + '\n')
            fd.close()

        return ob, reward, True, {}

    # Called when an environment is finished, creates a new "environment"
    def reset(self) -> list:
        return self._reset()

    # Rescale actions from [-1, 1] -> [0, 1] using a linear mapping
    @staticmethod
    def adjust_action(actions: np.ndarray) -> np.ndarray:
        return (actions + 1) / 2

    # return an observation
    def _reset(self) -> np.array:
        self.routes = make_demand_matrix(self.real_graph, self.demand_matrix_size)
        return self.routes

    # not used / doesn't make sense to use given the problem
    def _render(self, mode='human', close=False) -> None:
        pass

    # compute the reward
    def _get_reward(self) -> float:
        return compute_reward(self.real_graph, self.network_graph, self.routes)
