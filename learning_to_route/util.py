import networkx as nx
from copy import deepcopy
from random import choice, randint, random
from stable_baselines.common import BaseRLModel
import numpy as np
from statistics import mean
from stable_baselines.common.vec_env import DummyVecEnv

edge_update_order = []


# Create a graph that has 12 vertices and 30 edges
# origin paper had 12 vertices and 32 edges
def create_graph() -> nx.Graph:
    return randomize_weights(nx.icosahedral_graph())


# Randomly set the weights to be between [0.1,1.1]
def randomize_weights(graph: nx.Graph) -> nx.Graph:
    # Create a deep copy of the input graph
    random_edge_graph = deepcopy(graph)

    # if we don't have a master update list yes, make one
    if len(edge_update_order) == 0:
        for edge in random_edge_graph.edges:
            edge_update_order.append(edge)

    # for through all existing edges, remove it, then readd it with a random weight
    for edge in edge_update_order:
        random_edge_graph.remove_edge(edge[0], edge[1])
        random_edge_graph.add_edge(edge[0], edge[1], weight=random() + 0.1)
    return random_edge_graph


# Given a list of weights (an agent output), set the graph's weights per the order specified in master list
def set_weights(graph: nx.Graph, weights: np.ndarray) -> None:
    i = 0
    for edge in edge_update_order:
        graph[edge[0]][edge[1]]["weight"] = weights[i] + .01
        graph[edge[1]][edge[0]]["weight"] = weights[i] + .01
        i += 1


# Get the edges of a graph
def get_weights(graph: nx.Graph) -> list:
    edges = []
    for edge in edge_update_order:
        edges.append(graph[edge[0]][edge[1]]["weight"])
    return edges


def get_new_route(graph: nx.Graph) -> (int, int):
    node1 = None
    node2 = None
    nodes = list(graph.nodes)
    while node1 == node2:
        node1 = choice(nodes)
        node2 = choice(nodes)
    return node1, node2


# make a demand matrix of size 20 (node1, node2, number of times) but flatten it
def make_demand_matrix(graph: nx.Graph, size: int) -> np.array:
    routings = []
    demand_matrix = np.zeros(size)
    for i in range(0, size, 3):
        route_found = False
        while not route_found:
            node1, node2 = get_new_route(graph)
            if node1 != node2 and [node1, node2] not in routings:
                routings.append([node1, node2])
                routings.append([node2, node1])
                demand_matrix[i] = node1
                demand_matrix[i+1] = node2
                demand_matrix[i+2] = randint(1, 50)
                route_found = True
    return demand_matrix


# Normalize a list of values in format [node b, weight from node a to node b] to sum to 1
def normalize(values: list) -> list:
    total = 0.0
    normalized = []
    for value in values:
        total += value[1]
    for value in values:
        normalized.append([value[0], value[1] / total])

    return normalized


# Make a selection of which node to go to based on the normalized values of all weights leaving node a
def weighted_selection(values: list) -> int:
    # values are always a distribution that sum to 1
    random_value = random()
    for value in values:
        random_value -= value[1]
        if random_value <= 0:
            return value[0]


# Find the individual reward for routing between source and target
# reward = theoretical best path time / actual network time
def compute_path_reward(graph: nx.Graph, network_graph: nx.Graph, source: int, target: int) -> float:
    # nifty networkx function
    best_time = nx.astar_path_length(graph, source, target)

    # Start at source node
    # Select a neighbor node based on the weights, higher the weight relative to the others,
    #       more likely that node will be the next one
    # Repeat until reached target node, or until there have been n iterations (reward = -1, did not reach target)
    real_network_time = 0.0
    current_node = source
    prev_node = None
    depth = 0
    while current_node != target:
        neighbor_weights = []
        for edge in network_graph.edges(current_node):
            # if edge[1] == prev_node:
            #     continue
            neighbor_weights.append([edge[1], network_graph[edge[0]][edge[1]]["weight"]])
        normalized_weights = normalize(neighbor_weights)
        next_node = weighted_selection(normalized_weights)
        real_network_time += graph[current_node][next_node]["weight"]
        if next_node == target:
            break
        prev_node = current_node
        current_node = next_node

        # in case there is no route between source and target, have an exit condition
        depth += 1
        if depth > 25:
            return -1

    # best case is t / t = 1, but that won't be the case ever, it will be between on the smaller side of [0, 1]
    return best_time / real_network_time


# Compute the reward for a given network and routing rule graph
def compute_reward(graph: nx.Graph, network_graph: nx.Graph, routes: np.ndarray) -> float:
    total_reward = 0.0
    routes = np.reshape(np.array(routes), (int(len(routes) / 3), 3))
    for route in routes:
        reward = compute_path_reward(graph, network_graph, route[0], route[1]) * route[2]
        total_reward += reward
    return total_reward


# Enjoy trained agent
def evaluate_learning_to_route(model: BaseRLModel, env: DummyVecEnv) -> None:
    good = 0
    bad = 0
    good_rewards = []
    for i in range(200):
        done = False
        obs = env.reset()
        reward = 0
        while not done:
            action, _states = model.predict(obs)
            obs, reward, done, info = env.step(action)
        if done:
            if reward > 0:
                good += 1
                good_rewards.append(float(reward[0]))
            else:
                bad += 1
    print(f"% Routed: {good / float(good + bad)}")
    print(f"Average positive rewards: {mean(good_rewards)}")
