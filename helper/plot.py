import csv
import matplotlib.pyplot as plt
import glob as glob
import sys
import os


def get_files(directory: str) -> list:
    return glob.glob(directory + "/*.csv")


def parse_csv(file: str) -> list:
    data = []
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count != 0 and int(row[0]) <= int(sys.argv[3]):
                rewards = []
                for value in row[1:]:
                    rewards.append(float(value))
                data.append([int(row[0]), rewards])
                line_count += 1
            else:
                line_count += 1
    return data


def smooth_data(data: list) -> (list, list):
    smoothed_data = []
    for _ in data[0][1]:
        smoothed_data.append([])
    time_steps = []
    lasts = []
    for value in data[0][1]:
        lasts.append(value)
    for i in range(1, len(data)):
        for j, value in enumerate(data[i][1]):
            smoothed_val = lasts[j] * float(sys.argv[2]) + (1 - float(sys.argv[2])) * value  # Calculate smoothed value
            smoothed_data[j].append(smoothed_val)  # Save it
            lasts[j] = smoothed_val
        time_steps.append(data[i][0])
    return time_steps, smoothed_data


if sys.argv[1] == "-h":
    print("Please specify the directory of runs, as well as the smoothing factor [0. 1),"
          "the number of iterations, and the title of the graph")

run_info = []
time_steps = []
smooth = []
legend = []
plt.xlabel("Steps")
plt.ylabel("Reward")
n = 150
markers = ['x', 'v', '^', '+', "."]
# plt.title(sys.argv[4].replace("-", " "))
for i, run in enumerate(get_files(sys.argv[1])):
    time_step, smoothed = smooth_data(parse_csv(run))
    for smooth in smoothed:
        legend.append(os.path.split(run)[1][:-4].upper())
        plt.plot(time_step[::n], smooth[::n], markers[i], linestyle='-', markersize=7)

plt.legend(legend, loc='lower right')
plt.savefig("figures/" + sys.argv[4] + ".png")


