import networkx as nx

# Compute the reward
def compute_reward(graph: nx.Graph, target: int, path: list) -> (list):
    return 1

# Compute the reward of from the environment given a history of actions (the path)
def compute_reward_avoid(graph: nx.Graph, flow1_path: list, flow1_target: int, flow2_source: int, flow2_target: int) -> (float):
    return 1